using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets.Characters.FirstPerson
{
    [Serializable]
    public class MouseLook
    {
        public float XSensitivity = 2f;
        public float YSensitivity = 2f;
        public bool clampVerticalRotation = true;
        public Vector2 verticalRotationClamps = new Vector2(-90f, 90f);
        public bool clampHorizontalRotation = true;
        public Vector2 horizontalRotationClamps = new Vector2(-90f, 90f);
        //public float MinimumX = -90F;
        //public float MaximumX = 90F;
        public bool smooth;
        public float smoothTime = 5f;
        public float wallRunTilt = 0f;

        private Quaternion charTargetRot;
        private Quaternion camTargetRot;
        private Vector2 mouseInput, mouseLook;

        public void Init(Transform character, Transform camera)
        {
            charTargetRot = character.localRotation;
            camTargetRot = camera.localRotation;
        }

        public void LookRotation(Transform character, Transform camera)
        {
            mouseInput = InputManager.instance.mouseMovement;

            Vector2 rots = new Vector2(
                mouseInput.x * XSensitivity,
                mouseInput.y * YSensitivity
                );

            mouseLook += rots;
            mouseLook.y = Mathf.Clamp(mouseLook.y, -90f, 90f);

            //camTargetRot = Quaternion.AngleAxis(-mouseLook.y, Vector2.right);
            //charTargetRot = Quaternion.AngleAxis(mouseLook.x, Vector3.up);

            charTargetRot *= Quaternion.Euler(0f, rots.x, 0f);
            charTargetRot = Quaternion.Euler(0f, charTargetRot.eulerAngles.y, wallRunTilt);
            camTargetRot *= Quaternion.Euler(-rots.y, 0f, 0f);

            if (clampVerticalRotation) camTargetRot = ClampRotationAroundXAxis(camTargetRot);
            if (clampHorizontalRotation) charTargetRot = ClampRotationAroundYAxis(charTargetRot);

            if (smooth)
            {
                character.localRotation = Quaternion.Slerp(character.localRotation, charTargetRot,
                    smoothTime * Time.deltaTime);
                camera.localRotation = Quaternion.Slerp(camera.localRotation, camTargetRot,
                    smoothTime * Time.deltaTime);
            }
            else
            {
                character.localRotation = charTargetRot;
                camera.localRotation = camTargetRot;
            }

        }

        Quaternion ClampRotationAroundXAxis(Quaternion rotation)
        {
            rotation.x /= rotation.w;
            rotation.y /= rotation.w;
            rotation.z /= rotation.w;
            rotation.w = 1.0f;

            float angleX = 2.0f * Mathf.Rad2Deg * Mathf.Atan(rotation.x);

            angleX = Mathf.Clamp(angleX, verticalRotationClamps.x, verticalRotationClamps.y);

            rotation.x = Mathf.Tan(0.5f * Mathf.Deg2Rad * angleX);

            return rotation;
        }

        private Quaternion ClampRotationAroundYAxis(Quaternion rotation)
        {
            Vector3 eulers = rotation.eulerAngles;
            float angleY = eulers.y;

            if (angleY > 180f) angleY = angleY - 360f;

            angleY = Mathf.Clamp(angleY, horizontalRotationClamps.x, horizontalRotationClamps.y);

            rotation = Quaternion.Euler(eulers.x, angleY, eulers.z);

            return rotation;
        }

    }
}
