﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * TODO: 
 * -Infinite starting treadmill
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MusicGeneration;
using TrackGeneration;
using System;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public GameManagerParameters parameters;

    public float playStartTime = 0f;

    public bool gameStarted = false;
    public bool playing = false;
    public bool paused = false;

    MusicManager musicManager;
    TrackManager trackManager;
    CanvasManager canvasManager;
    CharacterManager charManager;
    EffectsManager effectsManager;
    EnvironmentManager environmentManager;
    GameManagerProperties properties;
    DangersManager dangersManager;
    InputManager inputManager;
    SoundEffectsManager soundFXManager;

    [Header("Debug")]
    public bool debugMessages;

    private void Awake()
    {
        if (instance != null && instance != this) Destroy(gameObject);
        else instance = this;

        properties = parameters.properties;
    }

    private void Start()//TODO: move to a "loading level" function
    {
        LoadLevel();
    }

    async private void LoadLevel()
    {
        gameStarted = false;

        musicManager = MusicManager.instance;
        trackManager = TrackManager.instance;
        canvasManager = CanvasManager.instance;
        effectsManager = EffectsManager.instance;
        environmentManager = EnvironmentManager.instance;
        dangersManager = DangersManager.instance;
        inputManager = InputManager.instance;
        soundFXManager = SoundEffectsManager.instance;

        trackManager.Initialize();
        await musicManager.GenerateSongAsync();
#if UNITY_EDITOR
        if (debugMessages) Debug.Log("GAME MANAGER: Song generated");
#endif

        await trackManager.GenerateDataTrackAsync();
#if UNITY_EDITOR
        if (debugMessages) Debug.Log("GAME MANAGER: Track Data generated.");
#endif

        await trackManager.GeneratePhysicalTrack();
        dangersManager.Initialize();
#if UNITY_EDITOR
        if (debugMessages) Debug.Log("GAME MANAGER: Physical Track generated.");
#endif
        //await new WaitForSeconds(1);
        //TODO: play song in synch with player

        environmentManager.GenerateBuildings(trackManager);

        charManager = Instantiate(properties.characterMovementPrefab);
        charManager.Initialize();

        canvasManager.Initialize(charManager);

        charManager.virtualPosition = -trackManager.properties.trackDirection * properties.secondsBeforeSongStart * 0.5f * trackManager.properties.MetersPerSecond;
        //canvasManager.BlackFade(false);

        soundFXManager.Initialize();
        effectsManager.Initialize(charManager, musicManager);
        effectsManager.BlackFade(false);


        gameStarted = true;
        playing = true;
        playStartTime = Time.time;

        StartCoroutine(PauseChecking());

        await new WaitForSeconds(properties.secondsBeforeSongStart - musicManager.generationProperties.startSongTimeBuffer);

        musicManager.PlaySong();
    }

    private void Update()
    {
        if (!gameStarted) return;

        trackManager.InverseMoveCharacter(charManager.virtualPosition);
        canvasManager.UpdateCanvas(charManager);
    }

    public void EndGameWin()
    {
        effectsManager.BlackFade(true,
            () =>
            {
                DeactivateCharacter();
                canvasManager.SetWinGamePanel(true);
            });

        playing = false;
    }

    public void EndGameLoss()
    {
        effectsManager.BlackFade(true, 
            () =>
            {
                DeactivateCharacter();
                canvasManager.SetLoseGamePanel(true);
            });

        playing = false;
    }

    public void DeactivateCharacter()
    {
        charManager.FSMController.enabled = false;
        inputManager.SetMouseLock(false);
        //canvasManager.SetLoseGamePanel(true);
    }

    public void PauseGameSwitch(bool setPaused)
    {
        Time.timeScale = setPaused ? 0f : 1f;

        paused = setPaused;
        playing = !paused;
        canvasManager.SetPauseGamePanel(setPaused);
        musicManager.PauseSongSwitch(setPaused);
        if (!paused) inputManager.SetMouseLock(true);
    }

    public void ResumeGame()
    {
        PauseGameSwitch(false);
    }

    private IEnumerator PauseChecking()
    {
        while (true)
        {

            if (Input.GetButtonDown("Cancel"))
            {
                PauseGameSwitch(!paused);
            }

            yield return new WaitForSecondsRealtime(0f);
        }
    }

    private void DisposeScene()
    {
        effectsManager.DisposeEffects();
        musicManager.StopSong();
        musicManager.UnloadSong();

        Time.timeScale = 1f;
    }

    async public void LoadMainMenuScene()
    {

        canvasManager.SetLoseGamePanel(false);
        await new WaitForSecondsRealtime(0.2f);

        DisposeScene();
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }

    public void ReloadGameScene()
    {
        DisposeScene();

        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }
}
