﻿/*
 * Copyright (c) 2015 Allan Pichardo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *  http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

using UnityEngine;
using System;
using PizanaUtility;

public class ExampleAudio : MonoBehaviour
{
    private const string colorSlot = "_Color";
    public float barHeight = 10f, maxColorMultiplier = 1.5f, colorDecreasePercPerSecond = 0.2f;
    public Material skyboxMat;


    public Color initialColor = Color.black;

    Color targetColor;

    void Start()
    {
        //Select the instance of AudioProcessor and pass a reference
        //to this object
        RealtimeAudioProcessor processor = FindObjectOfType<RealtimeAudioProcessor>();
        processor.onBeat.AddListener(onOnbeatDetected);
        processor.onSpectrum.AddListener(onSpectrum);

        if (RenderSettings.skybox != null)
            skyboxMat = RenderSettings.skybox;

        //initialColor = skyboxMat.GetColor(colorSlot);
    }

    private void Update()
    {
        //Debug.Log(skyboxMat.GetColor(colorSlot));
        targetColor = skyboxMat.GetColor(colorSlot);

        targetColor = Color.Lerp(targetColor, initialColor, colorDecreasePercPerSecond * Time.deltaTime);

        skyboxMat.SetColor(colorSlot, targetColor);
    }

    //this event will be called every time a beat is detected.
    //Change the threshold parameter in the inspector
    //to adjust the sensitivity
    void onOnbeatDetected()
    {
        //Debug.Log ("Beat!!!");

        targetColor = MathHelper.ClampVectorValues(initialColor + Color.white * maxColorMultiplier, 0, 1); ;

        skyboxMat.SetColor(colorSlot, targetColor);
    }

    //This event will be called every frame while music is playing
    void onSpectrum(float[] spectrum)
    {
        //The spectrum is logarithmically averaged
        //to 12 bands

        for (int i = 0; i < spectrum.Length; ++i)
        {
            Vector3 start = new Vector3(i, 0, 0);
            Vector3 end = new Vector3(i, spectrum[i] * barHeight, 0);
            Debug.DrawLine(start, end);
        }
    }
}
