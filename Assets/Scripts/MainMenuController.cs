﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;
using MusicGeneration;
using System;

[RequireComponent(typeof(MusicManager))]
public class MainMenuController : MonoBehaviour
{
    public PostProcessVolume mainMenuVolume;
    public MusicManager musicManager;

    [Header("Environment Control")]
    public EnvironmentManager envManager;
    public Color baseEmmColor = Color.blue;
    public float centerGlowingIntensity = 1f;
    public float glowingPushStrength = 1f;
    public Material[] buildingsGlowingMaterials;

    [Header("Bloom Variation")]
    public SoundDrivenValueDampened bloomValueVariation;
    float bloomValue = 0f;
    //public float bloomIntensityVariation = .1f;
    //public float bloomIncreaseThreshold = 0.5f;
    //public float bloomBaseIntensity = 0.3f;
    //public float bloomRecovery;
    //public float bloomPush;

    [Header("ChromaticAberration variation")]
    public SoundDrivenValueDampened chrAbrValueVariation;
    float chrAbrValue = 0f;
    //public float chromaticAberrationVariation = .1f;
    //public float chrAbrIncreaseThreshold = 0.5f;
    //public float chrAbrBaseIntensity = 0.3f;

    PostProcessProfile profile;

    Bloom bloom;
    ChromaticAberration chrAberr;

    Color[] initialColors;

    const string TAG_EMMISSION = "_EmissionColor";

    private void Awake()
    {

    }

    async private void Start()
    {
        musicManager = MusicManager.instance;


        profile = mainMenuVolume.profile;
        if (!profile.TryGetSettings(out bloom)) throw new UnityException("Main menu profile does not have a Bloom setting");
        if (!profile.TryGetSettings(out chrAberr)) throw new UnityException("Main menu profile does not have a Chromatic Aberration setting");

        bloomValueVariation.Initialize(bloom.intensity);
        //bloomCenterIntensity = bloom.intensity;
        //bloomBase = bloomCenterIntensity * bloomBaseIntensity;

        chrAbrValueVariation.Initialize(chrAberr.intensity);
        //chrAbrCenterIntensity = chrAberr.intensity;
        //chrAbrBase = chrAbrCenterIntensity * chrAbrBaseIntensity;

        initialColors = new Color[buildingsGlowingMaterials.Length];

        for (int i = 0; i < initialColors.Length; i++)
        {
            initialColors[i] = buildingsGlowingMaterials[i].GetColor(TAG_EMMISSION);
        }

        envManager.GenerateBuildings();

        await musicManager.GenerateSongAsync();

        musicManager.PlaySong();
    }

    public float melodyIntens, baseIntens;

    private void Update()
    {
        float playbackTime = musicManager.playbackTime;
        if (playbackTime < 0f) playbackTime = 0f;

        melodyIntens = musicManager.Song.GetInstantIntensityInLayer(MusicLayer.MELODY, playbackTime);
        bloomValue = bloomValueVariation.Update(bloom.intensity,baseIntens, Time.deltaTime);

        baseIntens = musicManager.Song.GetInstantIntensityInLayer(MusicLayer.BASE, playbackTime);
        chrAbrValue = chrAbrValueVariation.Update(chrAberr.intensity, baseIntens, Time.deltaTime);

        //TODO
        bloom.intensity.Override(bloomValue);
        chrAberr.intensity.Override(chrAbrValue);

        ModifyBuildingGlowing(playbackTime);
    }

    private void ModifyBuildingGlowing(float playbackTime)
    {
        float extraGlow = musicManager.Song.GetInstantIntensityInLayer(MusicLayer.MELODY, playbackTime) * glowingPushStrength;

        for (int i = 0; i < buildingsGlowingMaterials.Length; i++)
        {
            buildingsGlowingMaterials[i].SetColor(TAG_EMMISSION, baseEmmColor * (centerGlowingIntensity + extraGlow));
        }
    }

    void RestartMaterials()
    {
        for (int i = 0; i < buildingsGlowingMaterials.Length; i++)
        {
            buildingsGlowingMaterials[i].SetColor(TAG_EMMISSION, initialColors[i]);
        }
    }

    public void LoadGameScene()
    {
        RestartMaterials();
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    private void OnDestroy()
    {
        RestartMaterials();
    }

    public void ExitGame()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
                Application.Quit();
#endif
    }
}
