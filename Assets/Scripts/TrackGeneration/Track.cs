﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MusicGeneration;

namespace TrackGeneration
{
    [System.Serializable]
    public class Track
    {
        public TrackParameters parameters;
        public TrackManager parent;

        public Track(TrackParameters parameters, TrackManager parent)
        {
            this.parameters = parameters;
            this.parent = parent;
        }

        public void MoveTrack(Vector3 distance)
        {
            parent.transform.position += distance;
        }
    }

    [System.Serializable]
    public struct TrackParameters
    {
        public float[] intensities;
        public float averageIntensity;
        public float intervalTileLength;

        //public List<TrackDangerData> dangersList;
        public TrackTileParameters[] trackTilesData;
        public ISong parentSong;

        public TrackParameters(float[] intensities, float averageIntensity, float intervalTileLength, TrackTileParameters[] trackData, ISong parentSong)
        {
            this.intensities = intensities;
            this.averageIntensity = averageIntensity;
            this.intervalTileLength = intervalTileLength;
            this.trackTilesData = trackData;
            this.parentSong = parentSong;
        }
    }
}