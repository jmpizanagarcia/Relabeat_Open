﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MusicGeneration;

namespace TrackGeneration
{
    [CreateAssetMenu(menuName = "Track Generation/ Track Generation Parameters")]
    public class TrackGenerationParameters : ScriptableObject
    {
        public TrackGenerationProperties properties;
    }

    [Serializable]
    public struct TrackGenerationProperties
    {
        [Header("Difficulty level"), Tooltip("Average dangers generated per second of play. Different difficulty levels will have different values.")]
        public float avgDangersPerSecond;

        [Tooltip("Maximum allowed difficulties in a second expressed as a multiple of Average Dangers per Second. Depends on difficulty level")]
        public float maxDangersMultiple;

        [Tooltip("Maximum simultaneous dangers. Depends on difficulty level")]
        public float maxDangersPerTile;

        //[HideInInspector]
        //public float duration;

        [Tooltip("The length of a tile that represents the span of a beat. Longer tiles mean less vision and more difficulty.")]
        public float metersPerBeat;

        [Tooltip("Transversal dimension of the tiles, not counting floor and walls thickness.")]
        public Vector2 tileSectionDimensions;

        [Header("Dangers")]
        public int maxSameDangersTogether;

        [Tooltip("Dangers that can be generated in this track. Different difficulty levels will have different arrays of generable dangers.")]
        public TrackDangerDictionary generableDangers; //TODO: change to weighted list or array

        [Header("Generation distribution"), Tooltip("The amount of divisions analyzed for each beat. Default is 4. It may depend on difficulty")]
        public int intervalsPerBeat;

        [Tooltip("How much dangers should be prone to appear more at the start of bars"), Range(0f, 1f)]
        public float barInitPush;

        [Tooltip("How much dangers should be prone to appear on more intense intervals only"), Range(0f, 1f)]
        public float sharpenPush;

        [Tooltip("Curve that sets the chance for generating dangers depending on the amount of current dangers in the last second. 0.5 is avgDangersPerSecond.")]
        public AnimationCurve dangersAmountGenerationCurve;

        [Tooltip("Curve that sets the chance for generating dangers depending on the normalized intensity of the current interval.")]
        public AnimationCurve intensityGenerationCurve;

        [Tooltip("How much will matter previous danger distribution front to song intensity when placing dangers. 0 means just intensity, 1 means just dangers."), Range(0f, 1f)]
        public float dangersAmountToIntensityRatio;
        
        public ISong seedSong;

        [Header("Prefabs"), Tooltip("Tile that shapes itself into all of the others")]
        public TrackTile rootTile;

        public Transform initialTreadmillTile;

        public Transform finalTile;

        [Header("Other")]
        public Vector3 trackDirection;

        public TrackGenerationProperties(float avgDangersPerSecond, float maxDangersMultiple, float maxDangersPerTile, float metersPerBeat, TrackDangerDictionary generableDangers, TrackTile voidTile, int tilesPerBeat, float barInitPush, float sharpenPush, AnimationCurve dangersAmountGenerationCurve, AnimationCurve intensityGenerationCurve, float dangersAmountToIntensityRatio, SongComposed seedSong, int maxSameDangersTogether)
        {
            this.avgDangersPerSecond = avgDangersPerSecond;
            this.maxDangersMultiple = maxDangersMultiple;
            this.maxDangersPerTile = maxDangersPerTile;
            this.metersPerBeat = metersPerBeat;
            this.generableDangers = generableDangers;
            this.rootTile = voidTile;
            this.intervalsPerBeat = tilesPerBeat;
            this.barInitPush = barInitPush;
            this.sharpenPush = sharpenPush;
            this.dangersAmountGenerationCurve = dangersAmountGenerationCurve;
            this.intensityGenerationCurve = intensityGenerationCurve;
            this.dangersAmountToIntensityRatio = dangersAmountToIntensityRatio;
            this.seedSong = seedSong;
            this.maxSameDangersTogether = maxSameDangersTogether;

            this.secondsPerIntervalValue = -1f;
            this.metersPerIntervalValue = -1f;
            //this.length = -1f;
            //this.duration = seedSong.songParameters.duration;

            dangersAmountGenerationCurve.postWrapMode = WrapMode.ClampForever;
            intensityGenerationCurve.postWrapMode = WrapMode.ClampForever;
            trackDirection = Vector3.forward;
            tileSectionDimensions = Vector2.one;
            initialTreadmillTile = null;
            finalTile = null;
        }


        float secondsPerIntervalValue;
        public float SecondsPerInterval
        {
            get
            {
                if (secondsPerIntervalValue <= 0)
                    secondsPerIntervalValue = 60f / ((float)intervalsPerBeat * (float)seedSong.GetBPM());

                return secondsPerIntervalValue;
            }
        }

        public float IntervalsPerSecond
        {
            get
            {
                if (secondsPerIntervalValue <= 0)
                    secondsPerIntervalValue = 60f / ((float)intervalsPerBeat * (float)seedSong.GetBPM());

                return 1f / secondsPerIntervalValue;
            }
        }

        public float MetersPerSecond
        {
            get
            {
                return MetersPerInterval / SecondsPerInterval;
            }
        }

        public float MaxDangersPerSecondAllowed
        {
            get
            {
                return maxDangersMultiple * avgDangersPerSecond;
            }
        }

        private float metersPerIntervalValue;
        public float MetersPerInterval
        {
            get
            {
                if (metersPerIntervalValue <= 0)
                    metersPerIntervalValue = metersPerBeat / intervalsPerBeat;

                return metersPerIntervalValue;
            }
        }

        

        /*public float EvaluateDangerAmountCurve(float currentDangersPerSecond)
        {
            return dangersAmountGenerationCurve.Evaluate(currentDangersPerSecond / (maxDangersMultiple * avgDangersPerSecond));
        }

        public float EvaluateIntensityGenerationCurve(float currentIntensity, float maxIntensity)
        {
            return intensityGenerationCurve.Evaluate(currentIntensity / maxIntensity);
        }*/

        public float GetGenerationRandomThreshold(float currentDangersPerSecond, float currentIntensity, float maxIntensity)
        {
            //TODO: assure that curve results are between 0 and 1
            float dangersParameter = dangersAmountGenerationCurve.Evaluate(currentDangersPerSecond / (maxDangersMultiple * avgDangersPerSecond));
            float intensityParameter = intensityGenerationCurve.Evaluate(currentIntensity / maxIntensity);

            float value = dangersAmountToIntensityRatio * dangersParameter + (1f - dangersAmountToIntensityRatio) * intensityParameter;

            //Debug.Log("Dangers evaluation: " + dangersParameter +
            //    " || Current DPS: " + currentDangersPerSecond + //" || MaxEvaluator: " + maxDangersMultiple * avgDangersPerSecond+
            //    "\nIntensity evaluation: " + intensityParameter +
            //    " || CurrentIntensity: " + currentIntensity + //" || Max Intensity: " + maxIntensity
            //    "\nTotal value: " + value
            //    );

            return value;
        }
    }

}
