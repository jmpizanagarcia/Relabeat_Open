﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * TODO:
 * - Dangers that last more than one tile
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using PizanaUtility;

namespace TrackGeneration
{
    public class TrackTile : MonoBehaviour
    {
        public Transform floorTrackPrefab, wallTrackPrefab, ceilingTrackPrefab, floorBarTrackPrefab;
        public Vector2 tileSectionDimensions = new Vector2(1, 1);
        public TrackTileParameters parameters;

        [Header("Editor Debug Parameters")]
        public Color wireColor = Color.white;

        public void InstantiatePhysical(TrackGenerationProperties generationParameters, int orderInBar)
        {
            /*Danger placement inside a track Tile
         * 
         * 1.- Each danger is placed && initialized
         * 2.- Depending on the dangers placed, the physical structure of the tile has to be placed (i.e.: floor, walls...). Each structure part does its own checking against dangers placed.
         */
            this.tileSectionDimensions = generationParameters.tileSectionDimensions;

            TrackDangerParameters[] dangersToGenerate = parameters.startingDangers;
            TrackDanger generatedDanger;

            if (dangersToGenerate != null)
            {
                for (int i = 0; i < dangersToGenerate.Length; i++)
                {
                    generatedDanger = generationParameters.generableDangers[dangersToGenerate[i].type];

                    generatedDanger = Instantiate(generatedDanger, transform);

                    generatedDanger.name = parameters.secondInSong + "-->" + generatedDanger.parameters.type.ToString();
                    generatedDanger.parentTrackTile = this;


                    //Vector3 dangerLocalScale = new Vector3(tileSectionDimensions.x, tileSectionDimensions.y, generationParameters.intervalTileLength);
                    //generatedDanger.transform.localScale = dangerLocalScale;

                    generatedDanger.Initialize(this, generationParameters);

                }
            }

            PlacePhysicalStructures(generationParameters, orderInBar);
        }

        private void PlacePhysicalStructures(TrackGenerationProperties generationProperties, int orderInBar)
        {
            Transform floorPrefab = orderInBar == 0 || orderInBar + 1 == generationProperties.intervalsPerBeat * 4 ?
                floorBarTrackPrefab : floorTrackPrefab;
            //Transform floorPrefab = floorTrackPrefab;

            if (parameters.generateFloor)
            {
                Vector3 floorLocalPosition = new Vector3
                    (0,
                    -0.5f * tileSectionDimensions.y - floorPrefab.GetComponent<MeshRenderer>().bounds.size.y * 0.5f,
                    0);

                Transform floor = Instantiate(floorPrefab, transform);
                floor.localPosition = floorLocalPosition;

                Vector3 floorLocalScale = new Vector3(tileSectionDimensions.x, 1f, generationProperties.MetersPerInterval);

                floor.localScale = (Quaternion.Inverse(floor.rotation) * floorLocalScale).Abs();

                //if (orderInBar + 1 == generationProperties.intervalsPerBeat * 4)
                //{
                //    floor.rotation *= Quaternion.Euler(0f, 0f, 180f);
                //}

                if (orderInBar == 0)
                    floor.rotation *= Quaternion.Euler(0f, 0f, 180f);
                /*if (hasBar)
                {

                    Vector3 barPosition = transform.position - generationParameters.trackDirection.normalized * generationParameters.metersPerInterval * 0.5f;
                    Transform barMark = Instantiate(barMarkPrefab, barPosition, Quaternion.identity, transform);
                    barMark.localScale = new Vector3(tileSectionDimensions.x, tileSectionDimensions.y, 1f);
                }*/
            }
        }


        protected void OnDrawGizmosSelected()
        {
#if UNITY_EDITOR

            Vector3 bottomLeft = transform.position + (Vector3)(transform.localToWorldMatrix * new Vector3(-tileSectionDimensions.x * 0.5f, -tileSectionDimensions.y * 0.5f, 0)),
                bottomRight = transform.position + (Vector3)(transform.localToWorldMatrix * new Vector3(tileSectionDimensions.x * 0.5f, -tileSectionDimensions.y * 0.5f, 0)),
                topLeft = transform.position + (Vector3)(transform.localToWorldMatrix * new Vector3(-tileSectionDimensions.x * 0.5f, tileSectionDimensions.y * 0.5f, 0)),
                topRight = transform.position + (Vector3)(transform.localToWorldMatrix * new Vector3(tileSectionDimensions.x * 0.5f, tileSectionDimensions.y * 0.5f, 0));

            Gizmos.color = wireColor;
            Gizmos.DrawLine(bottomLeft, topLeft);
            Gizmos.DrawLine(topLeft, topRight);
            Gizmos.DrawLine(topRight, bottomRight);
            Gizmos.DrawLine(bottomRight, bottomLeft);
#endif
        }

    }

    [System.Serializable]
    public struct TrackTileParameters
    {
        [Tooltip("Second of the song that this tile represents. If it is not placed on a track yet it should be -1")]
        public float secondInSong;

        public TrackDangerParameters[] startingDangers; //Dangers that start in this tile
                                                        //public TrackDangerData[] remainingDangers; //Dangers that started in previous tile and that remain in this one

        public bool generateFloor;

        public TrackTileParameters(float secondInSong, TrackDangerParameters[] startingDangers)
        {
            this.secondInSong = secondInSong;
            this.startingDangers = startingDangers;
            generateFloor = true;
        }
    }
}