﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlackHole : MonoBehaviour {

    public float triggerDistance = 5f;

    CharacterManager charManager;
    Camera cam;

    bool triggered = false,
        initialized = false ;

	// Update is called once per frame
	void Update ()
    {
        if (!initialized)
        {
            //We need to do this here as character is generated after track in GameManager's Start()
            cam = Camera.main;
            charManager = CharacterManager.instance;
        }

        transform.LookAt(cam.transform);
        
        if(!triggered)
        {
            if(Mathf.Abs(charManager.transform.position.z - transform.position.z) <= triggerDistance)
            {
                triggered = true;
                TriggerEffect();
            }
        }
	}

    private void TriggerEffect()
    {
        CharacterManager.instance.invulnerable = true;
        //EffectsManager.instance.parameters.distortVariator.active = false;
        EffectsManager.instance.DoCameraDistort(GameManager.instance.EndGameWin);
    }

    private void OnDrawGizmos()
    {
#if UNITY_EDITOR
        Gizmos.DrawCube(transform.position + Vector3.back * triggerDistance, Vector3.one * 0.2f);
#endif
    }
}
