﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace PizanaUtility
{
    public static class CollectionExtensions
    {
        private static System.Random rng = new System.Random();


        public static T GetRandom<T>(this List<T> list)
        {
            return list[Random.Range(0, list.Count)];
        }

        public static T GetRandom<T>(this T[] array)
        {
            return array[Random.Range(0, array.Length)];
        }

        public static T GetRandom<T, T1>(this Dictionary<T1, T> dictionary)
        {
            int position = Random.Range(0, dictionary.Count);
            IEnumerator e = dictionary.Values.GetEnumerator();

            for (int i = 0; i <= position; i++) e.MoveNext();//Less than or equal because the first enumerator is void

            return (T)e.Current;
        }

        public static T GetRandomWithExclusions<T, T1>(this Dictionary<T1, T> dictionary, List<T> exclusions)
        {
            T[] arr = new T[dictionary.Count];
            List<T> list;

            dictionary.Values.CopyTo(arr, 0);
            list = new List<T>(arr);

            for (int i = 0; i < list.Count; i++)
            {
                if (exclusions.Contains(list[i]))
                {
                    list.Remove(list[i]);
                }
            }

            return list.GetRandom();
        }

        public static T GetRandomWithExclusion1<T, T1>(this Dictionary<T1, T> dictionary, T exclusion)
        {
            T[] arr = new T[dictionary.Count - 1];

            //dictionary.Values.CopyTo(arr, 0);
            IEnumerator enumerator = dictionary.Values.GetEnumerator();
            enumerator.MoveNext();

            for (int i = 0, j = 0; i < dictionary.Values.Count; i++)
            {
                if (!((T)enumerator.Current).Equals(exclusion))
                {
                    arr[j] = (T)enumerator.Current;
                    j++;
                }
                enumerator.MoveNext();
            }

            return arr.GetRandom();
        }

        public static T GetRandomWithExclusion2<T, T1>(this Dictionary<T1, T> dictionary, T exclusion)
        {
            T[] arr = new T[dictionary.Count];
            List<T> list;

            dictionary.Values.CopyTo(arr, 0);
            list = new List<T>(arr);

            for (int i = 0; i < list.Count; i++)
            {
                if (!list[i].Equals(exclusion))
                {
                    list.Remove(list[i]);
                }
            }

            return list.GetRandom();
        }

        public static void ForEach<T>(this T[] array, Action<T> action)
        {
            for (int i = 0; i < array.Length; i++)
            {
                action(array[i]);
            }
        }

        public static void ForEach<T>(this Queue<T> queue, Action<T> action)
        {
            T[] array = queue.ToArray();

            for (int i = 0; i < array.Length; i++)
            {
                action(array[i]);
            }
        }

        /*
        /// <summary>
        /// Checks if an unsorted array contains certain element
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool Contains<T>(this T[] array, T element, RequireClass<T> missing = null) where T : class
        {
            int length = array.Length;

            for (int i = 0; i < length; i++)
                if (element.Equals(array[i])) return true;

            return false;
        }*/

        public static bool Contains(this string[] array, string element)
        {
            int length = array.Length;

            for (int i = 0; i < length; i++)
            {
                if (element == array[i]) return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if an unsorted array contains certain element
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="array"></param>
        /// <param name="element"></param>
        /// <returns></returns>
        public static bool Contains<T>(this T[] array, T element, RequireStruct<T> missing = null) where T : struct
        {
            int length = array.Length;

            for (int i = 0; i < length; i++)
                if (array[i].Equals(element)) return true;
            return false;
        }

        public static void Shuffle<T>(this IList<T> list)
        {
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public static void Shuffle<T>(this T[] array)
        {
            int n = array.Length;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = array[k];
                array[k] = array[n];
                array[n] = value;
            }
        }

        /// <summary>
        /// Returns a random, non-repeated subset of elements in a list.
        /// This does not guarantee elements on the list are, by itself, repeated.
        /// </summary>
        /// <param name="list"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static T[] GetRandomSubset<T>(this List<T> list, int amount)
        {
            if (amount > list.Count) throw new UnityException("Trying to get a subset of a list greater than its number of elements");
            if (amount == list.Count) return list.ToArray();

            T[] returnValue = new T[amount],
                baseElements = list.ToArray();

            baseElements.Shuffle();

            for (int i = 0; i < amount; i++)
            {
                returnValue[i] = baseElements[i];
            }

            return returnValue;

        }

        /// <summary>
        /// Returns many values from the stack at once, ordered by LIFO
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="stack"></param>
        /// <param name="amount"></param>
        /// <returns></returns>
        public static T[] PopMany<T>(this Stack<T> stack, int amount)
        {
            if (stack.Count < amount)
            {
                throw new UnityException("Trying to pop more elements (" + amount + ") off a stack (" + stack.ToString() + ") that there actually are (" + stack.Count + ")");
                //return stack.ToArray();
            }

            T[] newArray = new T[amount];

            for (int i = 0; i < amount; i++)
            {
                newArray[i] = stack.Pop();
            }

            return newArray;
        }

        public static Vector3 Abs(this Vector3 vector)
        {
            return new Vector3(
                Mathf.Abs(vector.x),
                Mathf.Abs(vector.y),
                Mathf.Abs(vector.z)
                );
        }
    }
}
