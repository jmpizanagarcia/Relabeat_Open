﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PizanaUtility
{
    public static class MathHelper
    {
        public static int Sign(this Vector3 vector, Vector3 axis)
        {
            Vector3 projection = Vector3.Project(vector, axis.normalized);
            //Debug.Log("Angle: " + Vector3.Angle(projection, axis.normalized));
            //Debug.Log("Cosine: " + Mathf.Cos(Mathf.Deg2Rad * Vector3.Angle(projection, axis.normalized)));
            //Debug.Log("Rounded: " + Mathf.RoundToInt(Mathf.Cos(Vector3.Angle(projection, axis.normalized))));
            int value =  Mathf.RoundToInt(Mathf.Cos(Mathf.Deg2Rad * Vector3.Angle(projection, axis.normalized)));

            return value == 0 ? 1 : value;
        }

        public static Vector3[] GetArcPoints(Vector3 origin, Vector3 destiny, int steps, float arcHeight)
        {
            Vector3[] result = new Vector3[steps];
            for (int i = 0; i < steps; i++)
            {
                result[i] = Bezier.GetPoint(origin, Vector3.up * arcHeight + (origin + destiny) / 2f, destiny, (float)i / (float)(steps - 1f));
            }
            return result;
        }

        public static Vector4 ClampVectorValues(Vector4 original, float minValue, float maxValue)
        {
            return new Vector4(
                Mathf.Clamp(original.x, minValue, maxValue),
                Mathf.Clamp(original.y, minValue, maxValue),
                Mathf.Clamp(original.z, minValue, maxValue),
                Mathf.Clamp(original.w, minValue, maxValue)
                );
        }

        public static int FindNearestBiggerPowerof2(float num)
        {
            return (int)Mathf.Ceil(Mathf.Log(num, 2f));
        }

        public static Vector3 squareVectorValues(Vector3 source)
        {
            return new Vector3(source.x * source.x, source.y * source.y, source.z * source.z);
        }

        public class BezierCurve : MonoBehaviour
        {

            public Vector3[] points;

            public void Reset()
            {
                points = new Vector3[] {
            new Vector3(1f, 0f, 0f),
            new Vector3(2f, 0f, 0f),
            new Vector3(3f, 0f, 0f)
        };
            }
        }

        public static int FindNearestGreaterMultipleOf(this int target, int baseMultiple)
        {
            if (baseMultiple == 0) return 0;

            int remainder = Mathf.Abs(target) % baseMultiple;

            if (remainder == 0) return target;

            return (target < 0 ? -Mathf.Abs(target) - remainder : target + baseMultiple - remainder);
        }

        public class BezierSpline : MonoBehaviour
        {

            public Vector3[] points;

            public Vector3 GetPoint(float t)
            {
                return transform.TransformPoint(Bezier.GetPoint(points[0], points[1], points[2], points[3], t));
            }

            public Vector3 GetVelocity(float t)
            {
                return transform.TransformPoint(
                    Bezier.GetFirstDerivative(points[0], points[1], points[2], points[3], t)) - transform.position;
            }

            public Vector3 GetDirection(float t)
            {
                return GetVelocity(t).normalized;
            }

            public void Reset()
            {
                points = new Vector3[] {
                new Vector3(1f, 0f, 0f),
                new Vector3(2f, 0f, 0f),
                new Vector3(3f, 0f, 0f),
                new Vector3(4f, 0f, 0f)
                };
            }
        }

        //From https://github.com/setchi/Unity-LineSegmentsIntersection/blob/master/Assets/LineSegmentIntersection/Scripts/Math2d.cs
        public static bool SegmentsIntersection(Vector2 p1, Vector2 p2, Vector2 p3, Vector3 p4, out Vector2 intersection)
        {
            //TODO: repasar esto para admitir comparacion con recta
            intersection = Vector2.zero;

            var d = (p2.x - p1.x) * (p4.y - p3.y) - (p2.y - p1.y) * (p4.x - p3.x);

            if (d == 0.0f)
            {
                return false;
            }

            var u = ((p3.x - p1.x) * (p4.y - p3.y) - (p3.y - p1.y) * (p4.x - p3.x)) / d;
            var v = ((p3.x - p1.x) * (p2.y - p1.y) - (p3.y - p1.y) * (p2.x - p1.x)) / d;

            if (u < 0.0f || u > 1.0f || v < 0.0f || v > 1.0f)
            {
                return false;
            }

            intersection.x = p1.x + u * (p2.x - p1.x);
            intersection.y = p1.y + u * (p2.y - p1.y);

            return true;
        }

        //From https://gist.github.com/battleguard/22de81d11544bd671945
        public static bool Intersects2D(Line l1, Line l2, out Vector2 intersect)
        {
            float firstLineSlopeX = l1.p1.x - l1.p0.x;
            float firstLineSlopeY = l1.p1.y - l1.p0.y;

            float secondLineSlopeX = l2.p1.x - l2.p0.x;
            float secondLineSlopeY = l2.p1.y - l2.p0.y;

            float s = (-firstLineSlopeY * (l1.p0.x - l2.p0.x) + firstLineSlopeX * (l1.p0.y - l2.p0.y)) / (-secondLineSlopeX * firstLineSlopeY + firstLineSlopeX * secondLineSlopeY);
            float t = (secondLineSlopeX * (l1.p0.y - l2.p0.y) - secondLineSlopeY * (l1.p0.x - l2.p0.x)) / (-secondLineSlopeX * firstLineSlopeY + firstLineSlopeX * secondLineSlopeY);

            if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
            {
                float intersectionPointX = l1.p0.x + (t * firstLineSlopeX);
                float intersectionPointY = l1.p0.y + (t * firstLineSlopeY);

                // Collision detected
                intersect = new Vector2(intersectionPointX, intersectionPointY);
                return true;
            }

            intersect = new Vector2();
            return false; // No collision
        }
    }

    /*Useful math functions of other scripts that no longer have a home
* 
* 
* ///Geths the position of an object for a given path and a set total ainmation time
private Vector3 GetPositionInPath(float time)
{
    Vector3 result;
    Vector3[] path = gNadeData.path;
    float t = time / gNadeData.travelTime;

    //We take profit of the equal separation of points over time
    //We take the bottom index for that time frame
    int index = Mathf.FloorToInt((float)(path.Length - 1) * t);

    if (index == path.Length) return path[path.Length];

    //Now we need to get the specific t for the time frame
    float timeStartWindow = gNadeData.travelTime * (float)index / ((float)path.Length-1f),
        timeEndWindow = gNadeData.travelTime * (float)(index + 1) / (float)(path.Length-1f);

    //new t is how much it advanced from timeStart divided by the whole timespan
    t = (t-timeStartWindow)/(timeEndWindow-timeStartWindow);

    result = (path[index] + path[index + 1]) * t; 

    return result;
}
*/

}
