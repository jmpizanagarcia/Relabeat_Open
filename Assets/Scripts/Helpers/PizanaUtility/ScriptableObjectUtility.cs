﻿#if UNITY_EDITOR
//Got from http://wiki.unity3d.com/index.php?title=CreateScriptableObjectAsset
using UnityEngine;
using UnityEditor;
using System.IO;
using System.Reflection;
namespace PizanaUtility
{
    public static class ScriptableObjectUtility
    {
        /// <summary>
        //	This makes it easy to create, name and place unique new ScriptableObject asset files.
        /// </summary>
        public static T CreateAsset<T>(string path, string name) where T : ScriptableObject
        {

            //string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (path == null || path == "")
            {
                throw new UnityException("Trying to create an asset with no path");
                //path = "Assets";
            }
            else if (Path.GetExtension(path) != "")
            {
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }

            path = "Assets" + path.MakePathRelative(Application.dataPath);
            //Debug.Log(path + "/" + name + ".asset");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                AssetDatabase.Refresh();
            }

            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/" + name + ".asset");

            T asset = ScriptableObject.CreateInstance<T>();
            AssetDatabase.CreateAsset(asset, assetPathAndName);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            //EditorUtility.FocusProjectWindow();
            //Selection.activeObject = asset;

            return asset;
        }

        public static T CreatePreInitAsset<T>(string path, string name, T preInitAsset) where T : ScriptableObject
        {

            //string path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (path == null || path == "")
            {
                throw new UnityException("Trying to create an asset with no path");
                //path = "Assets";
            }
            else if (Path.GetExtension(path) != "")
            {
                path = path.Replace(Path.GetFileName(AssetDatabase.GetAssetPath(Selection.activeObject)), "");
            }

            path = "Assets" + path.MakePathRelative(Application.dataPath);
            //Debug.Log(path + "/" + name + ".asset");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
                AssetDatabase.Refresh();
            }

            string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/" + name + ".asset");


            AssetDatabase.CreateAsset(preInitAsset, assetPathAndName);

            EditorUtility.SetDirty(preInitAsset);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
            EditorUtility.FocusProjectWindow();
            //Selection.activeObject = asset;

            return preInitAsset;
        }
    }
}
#endif

