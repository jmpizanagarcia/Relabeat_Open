﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class RelativisticCameraEffect : MonoBehaviour
{

    public Camera cam;

    private Matrix4x4 relativisticProjectionMatrix;

    public float baseFov = 60f;

    public Matrix4x4 Kvalues = Matrix4x4.zero;

    //public float cK = 1f, cX = 1f, cY = 1f;

    private float S00, S11, a, b, far, near, newFar, newNear, newFov;
    private PhysicsManager relativity;

    void Awake()
    {
        cam = GetComponent<Camera>();
        far = cam.farClipPlane; newFar = far;
        near = cam.nearClipPlane; newNear = near;
        newFov = baseFov;
        //cam.renders
    }

    private void Start()
    {
        relativity = PhysicsManager.instance;
    }

    void Update()
    {

        SetRelativisticProjection();

    }

    private void SetRelativisticProjection()
    {
        //Si va muy lento probar a no asignar matrix4x4.zero sino hacer todo 0s a mano para que el gc no se vuelva loco

        Vector3 originalRatios = relativity.relativisticRatios * relativity.relativisticSpatialEffect;

        //Rotamos los ratios cartesianos del origen de mundo al sistema de referencia de la cámara
        Vector3 cameraSpaceRatios = cam.transform.rotation * originalRatios;

#if UNITY_EDITOR
        if (cameraSpaceRatios.x > 1 || cameraSpaceRatios.y > 1 || cameraSpaceRatios.z > 1)
        {
            Debug.LogError("Camera Space Relativity Ratios hace at least one component > 1", this);
            Debug.Break();
        }
#endif

        //cameraSpaceRatios = Vector3.one - MathHelper.squareVectorValues(cameraSpaceRatios);

        cameraSpaceRatios = Vector3.one - cameraSpaceRatios * relativity.relativisticSpatialEffect;

        float relativisticRatio = 1f - relativity.relativisticRatio * relativity.relativisticSpatialEffect;
        //newFar = far * relativisticRatio;
        //newNear = near * relativisticratio;
        //float newFov = baseFov / (relativisticRatio * relativisticRatio);

        SetUsualProjectionMatrix(relativisticRatio, newFov);

        /*
        Matrix4x4 relatScale = Matrix4x4.identity;
        relatScale.m00 = cameraSpaceRatios.x;
        relatScale.m11 = cameraSpaceRatios.y;
        relatScale.m22 = cameraSpaceRatios.z;

        relativisticProjectionMatrix = relativisticProjectionMatrix * relatScale;*/


        /*
        relativisticProjectionMatrix = Matrix4x4.zero;
        relativisticProjectionMatrix.m00 = S11 * cameraSpaceRatios.x;
        relativisticProjectionMatrix.m11 = S22 * cameraSpaceRatios.y;
        relativisticProjectionMatrix.m22 = a * cameraSpaceRatios.z - 1;
        relativisticProjectionMatrix.m33 = b*cameraSpaceRatios.z;
        */

        //Debug.Log(cam.projectionMatrix);
        //Debug.Log(relativisticProjectionMatrix);

        cam.projectionMatrix = relativisticProjectionMatrix;
    }



    private void SetUsualProjectionMatrix(float relativisticRatio, float newFov)
    {
        S11 = 1 / Mathf.Tan(newFov * 0.5f * Mathf.Deg2Rad);
        S00 = S11 / cam.aspect;

        a = -(newFar + newNear) / (newFar - newNear);
        b = -2 * newFar * newNear / (newFar - newNear);


        relativisticProjectionMatrix = Matrix4x4.zero;
        relativisticProjectionMatrix.m00 = S00;
        relativisticProjectionMatrix.m11 = S11;
        relativisticProjectionMatrix.m22 = a;
        relativisticProjectionMatrix.m23 = b;
        relativisticProjectionMatrix.m32 = /*-1/relativisticRatio*/-Kvalues.m32;


        relativisticProjectionMatrix.m01 = Kvalues.m01;
        relativisticProjectionMatrix.m02 = Kvalues.m02;
        relativisticProjectionMatrix.m03 = Kvalues.m03;
        relativisticProjectionMatrix.m10 = Kvalues.m10;
        relativisticProjectionMatrix.m12 = Kvalues.m12;
        relativisticProjectionMatrix.m13 = Kvalues.m13;
        relativisticProjectionMatrix.m20 = Kvalues.m20;
        relativisticProjectionMatrix.m21 = Kvalues.m21;
        relativisticProjectionMatrix.m30 = Kvalues.m30;
        relativisticProjectionMatrix.m31 = Kvalues.m31;
        relativisticProjectionMatrix.m33 = Kvalues.m33;

    }
}
