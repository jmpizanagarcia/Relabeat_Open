﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class RelativisticPostProcess : MonoBehaviour {

    public Material relativisticPostProcess;

    private void OnRenderImage(RenderTexture source, RenderTexture destination)
    {
        Graphics.Blit(source, destination, relativisticPostProcess);
    }
}
