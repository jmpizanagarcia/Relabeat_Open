﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;//TODO: todo
using UnityEngine.Rendering.PostProcessing;

[CreateAssetMenu(menuName = "Parameters/Character Parameters")]
public class CharacterParameters : ScriptableObject
{
    [Header("Game")]
    public int maxHealth = 3;
    public float maxEnergy = 3f;

    public float invulnerabilityTime = 1f;

    public float baseEnergyRegenPerSecond = 0.1f;
    public AnimationCurve energyRegenCurve;

    [Header("Skill Costs")]
    [Range(0f, 3f)]public float jumpCost = .33f;
    [Range(0f, 3f)]public float dashCost = 1f;
    [Range(0f, 3f)]public float crouchCostPerSecond = 1f;

    [Header("Physics")]
    public float jumpStrength = 10f;
    public float  rayRadius = .5f;
    [Range(0f, .5f)] public float groundedRaycastStart = 0.25f;
    [Range(0f, .5f)] public float groundedRaycastEnd = 0.5f;
    public float groundedDistanceBufferPercentage = 0.1f;
    public float minRaycastAdvance = 0.1f;

    public LayerMask collidableLayers;

    //public float gravityMultiplier = 1f;
    //public float strafeSpeedMultiplier = 0.5f;
    //public float strafeAcceleration = 1f;
    //public float stickToGroundForce = 0.001f;


    [Tooltip("How many seconds should it take for the character to get from 0 to track's speed")]
    public float forwardAccelerationTime = 1f;

    [Header("Animation")]
    public float stepsPerBeat = 2f;
    [Tooltip("How step interval should develop from 0 speed to max speed")]
    public AnimationCurve relativeSpeedToMaxSpeedStepAmount;

    [Header("Sound")]
    public AudioClip[] stepsSound;
}
