﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Decisions?/Wall Run?"), System.Serializable]
public class CharacterWallRunDecision : CharacterDecision
{
    protected override bool Assert(CharacterFSMController controller)
    {
        controller.characterManager.wallMovement.WallMovementCheck();
        return controller.characterManager.wallMovement.IsWallRunning;
    }
}
