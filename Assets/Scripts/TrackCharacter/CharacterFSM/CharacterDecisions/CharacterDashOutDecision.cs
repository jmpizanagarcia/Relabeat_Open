﻿using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Decisions?/Dash Out?"), System.Serializable]
public class CharacterDashOutDecision : CharacterDecision
{
    public float dashTime = 0.5f;

    protected override bool Assert(CharacterFSMController controller)
    {
        bool result =  controller.dashTimer >= dashTime;
        //Debug.Log("Dash Timer: " + controller.dashTimer + " || " + dashTime + " || " + result);
        //if (result)
        //{
        //    controller.dashTimer = 0f;
        //    controller.dashDirection = 0;
        //}

        return result;
    }
}