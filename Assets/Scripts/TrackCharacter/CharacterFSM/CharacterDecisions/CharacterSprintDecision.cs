﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Decisions?/Sprint?"), System.Serializable]
public class CharacterSprintDecision : CharacterDecision
{
    protected override bool Assert(CharacterFSMController controller)
    {
        throw new System.NotImplementedException();
        //TODO: set sprint button && place effects
    }
}
