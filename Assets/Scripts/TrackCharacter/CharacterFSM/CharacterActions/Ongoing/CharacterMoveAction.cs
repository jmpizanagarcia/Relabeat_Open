﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections;
using System.Collections.Generic;
using PizanaUtility.FSM;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Ongoing!/Move!"), System.Serializable]
public class CharacterMoveAction : CharacterOngoingAction
{
    public override void Act(CharacterFSMController controller)
    {
        //Run in the direction of the track
        controller.characterManager.virtualPosition += controller.characterManager.virtualSpeed * controller.deltaTime;
    }
}
