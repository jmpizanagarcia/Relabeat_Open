﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Ongoing!/Fall!"), System.Serializable]
public class CharacterFallAction : CharacterOngoingAction
{
    public float unpressedGravity = -10f, pressedGravity = -5f, maxFallingSpeedAbsolute = 20f, fallMass = 1f;

    Vector3 fallAxis, currentFallSpeed, speedIncrement;
    float speedIncrementMag, newFallSpeedMag;

    public override void Act(CharacterFSMController controller)
    {

        fallAxis = controller.characterManager.GetGravityDirection;
        currentFallSpeed = Vector3.Project(controller.characterManager.virtualSpeed, fallAxis);

        speedIncrement = fallAxis *
              controller.deltaTime 
             / (fallMass != 0f ? 1f / fallMass : 1f) //Mass effect jejejeje
             ;

        if (InputManager.instance.jumping && currentFallSpeed.Sign(fallAxis) >=0)
            speedIncrement *= pressedGravity;
        else
            speedIncrement *= unpressedGravity;

        newFallSpeedMag = (currentFallSpeed + speedIncrement).magnitude;

        if (newFallSpeedMag > maxFallingSpeedAbsolute)
        {
            //We use the unitary speed increment to keep the sign
            speedIncrement = speedIncrement.normalized * (maxFallingSpeedAbsolute - currentFallSpeed.magnitude);
            newFallSpeedMag = maxFallingSpeedAbsolute;
        }

        //dragVector = -fallAxis * newFallSpeedMag * drag;

        controller.characterManager.virtualSpeed += speedIncrement //+ dragVector;
            ;
    }
}
