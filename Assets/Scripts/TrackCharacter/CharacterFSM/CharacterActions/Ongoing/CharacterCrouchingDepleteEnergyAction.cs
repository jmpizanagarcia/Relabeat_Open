﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Ongoing!/Deplete Crouching Energy!")]
public class CharacterCrouchingDepleteEnergyAction : CharacterOngoingAction
{
    public override void Act(CharacterFSMController controller)
    {
        controller.characterManager.energy = Mathf.Max(0f,
            controller.characterManager.energy - controller.characterManager.parameters.crouchCostPerSecond * controller.deltaTime
            );
    }
}
