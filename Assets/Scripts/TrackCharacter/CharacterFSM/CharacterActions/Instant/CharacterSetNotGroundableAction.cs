﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="AI/Character FSM/Actions!/Instant!/Set Not Groundable!")]
public class CharacterSetNotGroundableAction : CharacterInstantAction
{
    public float seconds = 0.25f;

    public override void Act(CharacterFSMController controller)
    {
        controller.characterManager.MakeNotGroundable(seconds);
    }
}

	
