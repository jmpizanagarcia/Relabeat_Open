﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TrackGeneration;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Instant!/Adjust Vertical On Land!")]
public class CharacterAdjustVerticalOnLandAction : CharacterInstantAction
{
    [Tooltip("How much the character sinks in the platform to mantain a stable state")]
    public float securityBuffer = .001f;
    public override void Act(CharacterFSMController controller)
    {
        //Vector3 groundPoint;
        //controller.characterManager.CheckIfGrounded(out groundPoint);

        ////Y position adjustment on landing  TODO: check if necessary
        //Debug.Log(controller.characterManager.virtualPosition.ToString("F2"));
        ////Vector3 newHeightVector = controller.landPoint + Vector3.up * controller.characterManager.Height * 0.5f;
        //Vector3 virtualLandPoint = controller.landPoint + controller.characterManager.virtualPosition;
        //Debug.Log(controller.landPoint.ToString("F2") + " || " + virtualLandPoint.ToString("F2"));
        //Vector3 newPoint = virtualLandPoint + Vector3.up * controller.characterManager.Height * 0.5f;
        //Debug.Log(newPoint.ToString("F2"));
        ////controller.characterManager.virtualPosition.y = (newHeightVector - controller.characterManager.virtualPosition).y;
        ////controller.characterManager.virtualPosition.y = -(virtualLandPoint + Vector3.up * controller.characterManager.Height * 0.5f).y;
        ////controller.characterManager.virtualPosition.y = newPoint.y;

        controller.characterManager.virtualPosition.y = VerticalCharPosOnTrack();
    }

    public float VerticalCharPosOnTrack()
    {
        CharacterManager charManager = CharacterManager.instance;
        Vector3 charVPos = charManager.virtualPosition;
        float floorSurfVirtualPos = -.5f * TrackManager.instance.properties.tileSectionDimensions.y;
        float charGlobalHalfHeight = charManager.capsCollider.height * .5f;

        float newYPos = floorSurfVirtualPos + charGlobalHalfHeight - securityBuffer;
        //Debug.Log(floorSurfVirtualPos + " " + charGlobalHalfHeight + " " + newYPos);
        return newYPos;
    }

}
