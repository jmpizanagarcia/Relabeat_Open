﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "AI/Character FSM/Actions!/Instant!/Adjust Position On Running!")]
public class CharacterAdjustPositionOnRunningAction : CharacterInstantAction
{
    public override void Act(CharacterFSMController controller)
    {
        Vector3 groundPoint;
        controller.characterManager.CheckIfGrounded(out groundPoint);

        //Y position adjustment on landing  TODO: check if necessary
        Debug.Log(controller.characterManager.virtualPosition.ToString("F2"));
        //Vector3 newHeightVector = controller.landPoint + Vector3.up * controller.characterManager.Height * 0.5f;
        Vector3 virtualLandPoint = controller.landPoint + controller.characterManager.virtualPosition;
        Debug.Log(controller.landPoint.ToString("F2") + " || " + virtualLandPoint.ToString("F2"));
        Vector3 newPoint = virtualLandPoint + Vector3.up * controller.characterManager.Height * 0.5f;
        Debug.Log(newPoint.ToString("F2"));
        //controller.characterManager.virtualPosition.y = (newHeightVector - controller.characterManager.virtualPosition).y;
        //controller.characterManager.virtualPosition.y = -(virtualLandPoint + Vector3.up * controller.characterManager.Height * 0.5f).y;
        //controller.characterManager.virtualPosition.y = newPoint.y;
    }
}
