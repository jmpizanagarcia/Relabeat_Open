﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * 
 * */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

namespace MusicGeneration
{
    public class MusicManager : MonoBehaviour
    {
        public static MusicManager instance;

        public float playbackTime;//TODO: check if intensity analysis really reaches the end of the song // check audio arranging in intensity arrays on clips

        //public Slider loadBarSlider;

        public MusicGenerationParameters generationParametersObject;

        MusicGenerator generator;
        public ISong currentSong;
        bool hasSong = false;
        bool playing = false;
        [HideInInspector] public MusicGenerationProperties generationProperties;

        [Header("Debug")]
        public bool debugMessages = true;

        public ISong Song
        {
            get
            {
                return currentSong;
            }

            set
            {
                if (value == null) return;

                hasSong = true;
                currentSong = value;
            }
        }

        public bool Playing
        {
            get
            {
                return playing;
            }
        }

        private void Awake()
        {
            if (instance != null && instance != this) Destroy(gameObject);
            else instance = this;

            generationProperties = generationParametersObject.properties;

            generator = new MusicGenerator(this);
            hasSong = false;

        }

        public void LoadClipSong()
        {

        }

        async public Task<bool> GenerateSongAsync()
        {
            if (hasSong)
            {
                StopSong();
                UnloadSong();
            }

            hasSong = false;
            Song = await generator.GenerateSongAsync();

            return currentSong != null;
        }

        private void Update()
        {
            if (playing)
            {
                playbackTime += Time.deltaTime;

                Song.Update(playbackTime);
            }
        }

        public void PlaySong()
        {
            if (!hasSong) return;

            currentSong.Play();
            playing = true;

            playbackTime = -generationProperties.startSongTimeBuffer;
        }

        public void PauseSongSwitch(bool setPaused)
        {
            if (setPaused)
            {
                Song.Pause();
            }
            else
            {
                Song.Resume();
            }

            playing = !setPaused;
        }

        public void StopSong()
        {
            currentSong.Stop();
            playing = false;
            playbackTime = 0f;
        }

        public void UnloadSong()
        {
            hasSong = false;
            currentSong.Dispose();
            currentSong = null;

            //GC will take care of the actual previous song data destruction
        }

        public void SaveSong()
        {
            if (!hasSong) return;
            throw new NotImplementedException();
        }

        public void LoadSong()
        {
            hasSong = false;
            throw new NotImplementedException();
        }

        public void EndSwitchingSongSegments()
        {
#if UNITY_EDITOR
            if(debugMessages) Debug.Log("Switching segment at playback time: " + playbackTime);
#endif
            ((SongComposed)Song).EndSwitchingSongSegments();
        }

        public float GetInstantSongIntensities()
        {
            return Song.GetInstantIntensity(playbackTime);
        }
    }
}
