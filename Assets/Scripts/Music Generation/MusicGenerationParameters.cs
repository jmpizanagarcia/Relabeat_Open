﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace MusicGeneration
{
    [CreateAssetMenu(menuName = "Music Generation/ Music Generation Parameters")]
    public class MusicGenerationParameters : ScriptableObject
    {
        public MusicGenerationProperties properties;
    }

    [System.Serializable]
    public struct MusicGenerationProperties
    {
        public SampleLibrary library;

        public float desiredSongMinutes;

        public float maxSampleDuration;

        public AudioMixerGroup melodyMixer, harmonyMixer, beatMixer;
        public AudioSource audioSourcePrefab;

        public List<MusicMode> acceptedModes;

        [Tooltip("Multiplier mean to express hoy many times more samples we get regarding melodyAmounts for the whole song generation")]
        public int sampleBufferMultiplier;

        [Tooltip("How many modes in total are available for the song. More modes will result in more variation between segments. Default is 2")]
        public int maxModesAvailable;

        [Tooltip("The amounf of previous segment AudioSources that will be stored for possible future use")]
        public int previousSegmentSourcesStored;

        //[Tooltip("The fade curve between song segments.")]
        //public AnimationCurve segmentSwitchCurve;

        public float startSongTimeBuffer, nextSegmentTimeBuffer;

        public SegmentAmounts mainSegmentAmounts, introSegmentAmounts, bridgeSegmentAmounts, endingSegmentAmounts;

        [System.Serializable]
        public struct SegmentAmounts
        {
            public int minTotal; //TODO: introduce minimum total quantity
            public LayerSamplesAmounts baseAmounts, melodyAmounts, accoAmounts;
        }

        [System.Serializable]
        public struct LayerSamplesAmounts
        {
            public int min, max;
        }
    }
}