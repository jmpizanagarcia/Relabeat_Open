﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;

namespace MusicGeneration
{
    [CreateAssetMenu(fileName = "NewSampleLibrary", menuName = "Music Generation/Sample Library", order = 1), System.Serializable]
    public class SampleLibrary : ISamplesCollection<LibraryParameters>
    {
       
        /*
        public SampleLibrary Initialize(List<SampleFamilyCollection> mainLayerSamples, List<SampleFamilyCollection> baseLayerSamples, List<SampleFamilyCollection> accoLayerSamples)
        {
            this.mainLayerSamples = mainLayerSamples;
            this.baseLayerSamples = baseLayerSamples;
            this.accompanimentLayerSamples = accoLayerSamples;

            return this;
        }

        public static SampleLibrary InstantiateSampleLibrary(List<SampleFamilyCollection> mainLayerSamples, List<SampleFamilyCollection> baseLayerSamples, List<SampleFamilyCollection> accoLayerSamples, string path, string name)
        {
            return ScriptableObjectUtility.CreateAsset<SampleLibrary>(path, name).Initialize(mainLayerSamples, baseLayerSamples, accoLayerSamples);
        }*/
    }

    [System.Serializable]
    public class LibraryParameters : IParams
    {
        public SampleLayerCollection meloLayer;
        public SampleLayerCollection baseLayer;
        public SampleLayerCollection accoLayer;

        public LibraryParameters(SampleLayerCollection meloLayer, SampleLayerCollection baseLayer, SampleLayerCollection accoLayer)
        {
            this.meloLayer = meloLayer;
            this.baseLayer = baseLayer;
            this.accoLayer = accoLayer;
        }

        public LibraryParameters() { }
    }
}


