﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PizanaUtility;

namespace MusicGeneration
{
    [CreateAssetMenu(fileName = "NewSampleLayer", menuName = "Music Generation/Sample Layer", order = 1), System.Serializable]
    public class SampleLayerCollection : ISamplesCollection<LayerCollectionParams>
    {

        /*public override ISamplesCollection Initialize(IParams iParameters)
        {
            parameters.AssignNotNull((LayerCollectionParams)iParameters);

            parameters = (LayerCollectionParams)iParameters;

            //Intentional casting to throw an exception instead "as"'s null return
            LayerCollectionParams intendedParameters = (LayerCollectionParams)iParameters;

            parameters.layer = intendedParameters.layer;

            if (parameters.modesDictionary == null)
                throw new UnityException("Trying to initialize a Sample Layer Collection with no modes dictionary.");

            parameters.modesDictionary = intendedParameters.modesDictionary;


            if (parameters.modesDictionary == null)
            {
                if (parameters.modesList == null)
                    throw new UnityException("Trying to initialize a Sample Layer Collection with no modes list nor dictionary");

                parameters.modesDictionary = new Dictionary<MusicalMode, SampleModeCollection>(parameters.modesList.Count);

                parameters.modesList.ForEach((SampleModeCollection collection) => modesDic.Add(collection.mode, collection));
            }

            return this;
        }*/
    }

    [System.Serializable]
    public class LayerCollectionParams : IParams
    {
        public MusicLayer layer;
        public ModesDictionary modesDictionary;


        public LayerCollectionParams(MusicLayer layer, ModesDictionary modesDictionary)
        {
            this.layer = layer;
            this.modesDictionary = modesDictionary;
        }
        public LayerCollectionParams() { }
    }
    [System.Serializable]
    public class ModesDictionary : SerializableDictionary<MusicMode, SampleModeCollection> { }
}
