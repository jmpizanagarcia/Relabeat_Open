﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
 * TODO
*/

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace TrackGeneration
{
    public class LaserDynamicDanger : LaserDanger
    {
        [Header("Dynamic Laser Parameters")]
        public float angularSpeed;
        public float centerSpeed;

        Vector2 currentCenterSpeed;

        public override void Initialize(TrackTile tile, TrackGenerationProperties generationParameters)
        {
            base.Initialize(tile, generationParameters);

            currentCenterSpeed = Random.insideUnitCircle;
            angularSpeed *= Random.Range(0, 2) == 1 ? 1 : -1;
            laserLine.startColor = laserColor;
            laserLine.endColor = laserColor;
        }

        public void Update()
        {

            MoveLaser(Time.deltaTime);

        }

        public override void OnTriggerEnter(Collider other)
        {
            base.OnTriggerEnter(other);
        }

        private void MoveLaser(float deltaTime)
        {
            localAngle += angularSpeed * deltaTime;

            float validAreaHalfWidth = innerRectangleDimensionProportions.x * 0.5f,
                validAreaHalfHeight = innerRectangleDimensionProportions.y * 0.5f;

            Vector2 newCenter = localCenter + currentCenterSpeed.normalized * centerSpeed * deltaTime;

            // TODO: esto se puede comprimir obviamente
            if (newCenter.x < -validAreaHalfWidth)
            {
                currentCenterSpeed.x *= -1;
                //bounce distance
                newCenter.x = -validAreaHalfWidth + (Mathf.Abs(newCenter.x) - validAreaHalfWidth);
            }
            else if (newCenter.x > validAreaHalfWidth)
            {
                currentCenterSpeed.x *= -1;

                newCenter.x = validAreaHalfWidth - (newCenter.x - validAreaHalfWidth);
            }

            if (newCenter.y > validAreaHalfHeight)
            {
                currentCenterSpeed.y *= -1;

                newCenter.y = validAreaHalfHeight - (newCenter.y - validAreaHalfHeight);
            }
            else if (newCenter.y < -validAreaHalfHeight)
            {
                currentCenterSpeed.y *= 1;

                newCenter.y = -validAreaHalfHeight + (Mathf.Abs(newCenter.y) - validAreaHalfHeight);
            }

            localCenter = newCenter;

            SetEmittersPosition();
            SetLaserLinePositions();
            SetColliderPositions();
        }
    }
}

