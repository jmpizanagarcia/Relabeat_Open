﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TrackGeneration
{
    public class SilenceDanger : TrackDanger
    {
        public BoxCollider silenceArea;

        public override void Initialize(TrackTile tile, TrackGenerationProperties generationParameters)
        {

            Vector3 localScale = new Vector3(tile.tileSectionDimensions.x, tile.tileSectionDimensions.y, generationParameters.MetersPerInterval);
            transform.localScale = localScale;
        }

    }
}
