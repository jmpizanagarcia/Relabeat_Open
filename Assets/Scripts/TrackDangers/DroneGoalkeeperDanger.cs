﻿/*Made by Jose María Pizana, 2018 http://www.jmpizana.com
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

namespace TrackGeneration
{
    public class DroneGoalkeeperDanger : DroneDanger
    {
        [Header("Goalkeeper Drone Parameters")]
        public int followDelayBeats = 2;
        public int movementPeriodBeats = 3;
        public int markerFollowBeats = 4;
        public int beatsAhead = 1;
        public float movementDuration = 0.5f;

        [Header("Visual Elements")]
        public Color deactivatedMarkerColor = Color.blue;
        public Color activatedMarkerColor = Color.red;

        [Header("Prefabs")]
        public Image markerPrefab;
        Image marker;
        //Image markerChild;

        Vector2 lastSampledPosition;
        public float movementPeriod, followDelay, markerFollowPreTime;
        float movementTimer = 0f, samplingTimer = 0f;
        float virtualPosition;
        float nextSamplingTime = 0f;
        DangersManager dangersManager;
        bool switched = false, moved = false;
        public override void Initialize(TrackTile tile, TrackGenerationProperties generationParameters)
        {
            base.Initialize(tile, generationParameters);
            marker = Instantiate(markerPrefab
                //, CanvasManager.instance.worldCanvas.transform
                , DangersManager.instance.markersParent
                );
            marker.transform.position = transform.position;
            marker.color = deactivatedMarkerColor;
            marker.fillAmount = 0f;
            //marker.gameObject.SetActive(false);
            // markerChild = marker.GetComponentInChildren<Image>();
            //markerChild.gameObject.SetActive(false);
        }

        private void Awake()
        {
            if (movementPeriodBeats < followDelayBeats + beatsAhead)
            {
                enabled = false;
                throw new UnityException("Trying to use a goalkeeper with period lesser than delay.");
            }

            if (markerFollowBeats < movementPeriodBeats)
            {
                enabled = false;
                throw new UnityException("Trying to use a goalkeeper with Maker Follow lesser than Movement Period.");
            }

            movementTimer = 0f;
            samplingTimer = 0f;
            nextSamplingTime = movementPeriod - followDelay;
        }

        private void Start()
        {
            dangersManager = DangersManager.instance;
            //if (dangersManager.totalSampledBeats < followDelayBeats)
            //{
            //    enabled = false;
            //    throw new UnityException("Trying to use a goalkeeper with a follow delay greater than Danger Manager's total sampled beats");
            //}

            movementPeriod = movementPeriodBeats * dangersManager.secondsPerBeat;
            followDelay = (followDelayBeats + beatsAhead) * dangersManager.secondsPerBeat;
        }

        public void Update()
        {
            if (moved) return;

            float timeToChar = TimeToCharacter();

            if (!switched && Mathf.Abs(timeToChar) > followDelay)
            {
                SampleCharacterPosition();
                marker.transform.localPosition = (Vector3)lastSampledPosition + (Vector3.forward * marker.transform.localPosition.z);
            }
            else
            {
                if (!switched)
                {
                    marker.color = activatedMarkerColor;
                    switched = true;
                    movementTimer = followDelay;
                }

                movementTimer += Time.deltaTime;
                marker.fillAmount = (movementTimer + followDelay - movementPeriod) / followDelay;

                if (movementTimer > movementPeriod)
                {
                    marker.fillAmount = 1f;
                    //Activate some marker animation if any
                    Move();
                    moved = true;
                }
            }
        }

        /*public  void Update()
        {
            samplingTimer += Time.deltaTime;
            movementTimer += Time.deltaTime;

            if (marker.IsActive())
            {
                marker.fillAmount = (movementTimer + followDelay - movementPeriod) / followDelay;
            }

            if (samplingTimer > nextSamplingTime)
            {
                nextSamplingTime = float.PositiveInfinity;
                SampleCharacterPosition();

                marker.gameObject.SetActive(true);
                //markerChild.gameObject.SetActive(true);
                marker.transform.DOLocalMove((Vector3)lastSampledPosition + (Vector3.forward * marker.transform.localPosition.z), 0.1f);
            }

            if (movementTimer > movementPeriod)
            {
                //Debug.Log("lastsample: " + lastSampledPosition + " || current: " + );
                movementTimer = 0f;
                samplingTimer = 0f;
                marker.fillAmount = 1f;
                marker.gameObject.SetActive(false);
                Debug.Log(marker.IsActive());
                nextSamplingTime = movementPeriod - followDelay;
                // markerChild.gameObject.SetActive(false);
                Move();
            }

        }*/


        private void SampleCharacterPosition()
        {
            Vector2 globalPosition = CharManager.transform.position - parentTrackTile.transform.position;

            //We use the vector as placeholder
            lastSampledPosition = parentTrackTile.transform.worldToLocalMatrix * globalPosition;
            //lastSampledPosition = new Vector2(lastSampledPosition.x, lastSampledPosition.y);
        }

        private void Move()
        {

            //Vector2 newPos = new Vector2(lastSampledPosition.x,
            //    lastSampledPosition.y);

            //Vector2 locPos = transform.localPosition;
            //float duration = (newPos - (Vector2)locPos).magnitude / movementSpeed;
            //Debug.Log("locpos: " + locPos + " || destinyPos: " + newPos);
            //if (tw != null) tw.Kill(true);
            /*tw = DOTween.To(() => locPos, (Vector2 v) => locPos = v,
                newPos, duration);*/
            transform.DOKill(true);
            transform.DOLocalMove((Vector3)lastSampledPosition + Vector3.forward * transform.localPosition.z, movementDuration)
                .OnComplete(() => marker.fillAmount = 0f);
        }

        CharacterManager charManager;
        private CharacterManager CharManager
        {
            get
            {
                if (charManager == null) charManager = CharacterManager.instance;

                return charManager;
            }
        }
    }
}
