﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CityBottom : MonoBehaviour {

    Vector3 initialPosition;

    private void Awake()
    {
        initialPosition = transform.position;
    }

    private void Update()
    {
        transform.position = new Vector3(
                    transform.position.x,
                    initialPosition.y - CharacterManager.instance.virtualPosition.y,
                    transform.position.z);
    }
}
