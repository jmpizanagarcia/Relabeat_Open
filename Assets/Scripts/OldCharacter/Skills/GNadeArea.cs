﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class GNadeArea : MonoBehaviour
{

    //In this script we hope that forces in rigibodies are calculated as a single vector applied to it in order
    //to keep it optimal.
    [HideInInspector]
    public GNadeData gNadeData;

    SphereCollider coll;
    float time;
    List<Rigidbody> rBsInside;
    bool playerInside;

    private void Start()
    {
        coll = GetComponent<SphereCollider>();
        transform.localScale = Vector3.one * gNadeData.areaRadius;
        time = 0f;
        rBsInside = new List<Rigidbody>(25);
    }

    private void Update()
    {
        if (time >= gNadeData.areaDuration)
            EndGNadeArea();

        time += Time.deltaTime * Time.timeScale;
    }

    private void EndGNadeArea()
    {
        if (playerInside)
        {
            PlayerMovement.instance.currentGravity += -PhysicsManager.instance.originalGravity * gNadeData.areaGMultiplier;
        }

        for (int i = 0; i < rBsInside.Count; i++)
        {
            RemoveEffectFrom(rBsInside[i]);
        }
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {

        Vector3 gravForce = PhysicsManager.instance.originalGravity * gNadeData.areaGMultiplier;

        if (other.tag == "Player")
        {
            PlayerMovement player = other.GetComponent<PlayerMovement>();

            player.currentGravity += gravForce;
            playerInside = true;
            player.transform.position += Vector3.up * 0.1f;
        }
        else
        {
            Rigidbody rB = other.attachedRigidbody;
            if (rB == null) return;

            rBsInside.Add(rB);
            rB.AddForce(gravForce, ForceMode.Force);
        }       
    }

    private void OnTriggerExit(Collider other)
    {
        Vector3 gravForce = -PhysicsManager.instance.originalGravity * gNadeData.areaGMultiplier;

        if (other.tag == "Player")
        {
            PlayerMovement player = other.GetComponent<PlayerMovement>();
            player.currentGravity += gravForce;
            playerInside = false;
        }
        else
        {
            Rigidbody rB = other.attachedRigidbody;
            if (rB == null) return;

            RemoveEffectFrom(rB);
        }
    }

    private void RemoveEffectFrom(Rigidbody rB)
    {
        if (rB == null) return;

        rBsInside.Remove(rB);

        //We add a complementary force in order to return to the initial state of accelerations
        rB.AddForce(-PhysicsManager.instance.originalGravity * rB.mass * gNadeData.areaGMultiplier, ForceMode.Force);
    }

}
