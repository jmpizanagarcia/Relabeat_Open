﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public abstract class Skill{

    public float cooldown;
    public bool enabled;

    public abstract void Setup(SkillManager context);
    public abstract void Update(SkillManager context);

}
