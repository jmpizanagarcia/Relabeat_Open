﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhysicsManager : MonoBehaviour {

    public static PhysicsManager instance;

    public enum RelativityMethod
    {
        NONE, PER_OBJECT, PER_VERTEX, PER_VERTEX_COMPUTE_SHADER
    }

    public bool timeControl = true, spaceControl = true;

    public PlayerMovement referenceSystem;

    [Range(0f, 1f)]
    public float relativisticTimeEffect = 0.3f;

    [Range(0f, 1f)]
    public float relativisticSpatialEffect = 0.3f;

    [Range(0f, 1f), Tooltip("Secondary effect for per_object method")]
    public float relativisticScaleEffect = 0.3f;

    public float smoothRelativity = 5f;

    public float maxSpeedPercentage;

    public float minSignificantSpeed = 0.1f;

    public float scaledFixedDeltaTime = 0.0016f;

    public RelativityMethod relativityMethod;

    [HideInInspector]
    public float originalPhysicsTimeStep;
    public Vector3 originalGravity;

    public float relativisticRatio = 0;
    public Vector3 relativisticRatios; //A number between 0 and 1, usually

    [HideInInspector]
    public Vector3 referencePoint;

    private void Awake()
    {
        if (instance != null && instance != this) Destroy(this);

        instance = this;

        originalPhysicsTimeStep = Time.fixedDeltaTime;
        originalGravity = Physics.gravity;
        relativisticRatio = 0f;
    }

    private void Start()
    {
        referenceSystem = PlayerMovement.instance;
    }

    private void Update()
    {
        if (!timeControl && !spaceControl) return;

        /*Vector3 correctedSpeed = new Vector3(
            Mathf.Abs(referenceSystem.Velocity.x),
            Mathf.Abs(referenceSystem.Velocity.y),
            Mathf.Abs(referenceSystem.Velocity.z)
            );

        correctedSpeed = new Vector3(
            correctedSpeed.x > minSignificantSpeed ? correctedSpeed.x : 0,
            correctedSpeed.y > minSignificantSpeed ? correctedSpeed.y : 0,
            correctedSpeed.z > minSignificantSpeed ? correctedSpeed.z : 0
            );*/

        float lightSpeed = referenceSystem.forwardSpeed;
        Vector3 correctedSpeed = referenceSystem.activeSpeed;
        //Debug.Log(correctedSpeed);
        if(correctedSpeed.magnitude > lightSpeed)
        {
            correctedSpeed = correctedSpeed.normalized * lightSpeed;
        }

        relativisticRatio = Mathf.Lerp(relativisticRatio, Mathf.Min(Mathf.Abs(correctedSpeed.magnitude) / lightSpeed, 1f), smoothRelativity);

        //We do it value per value for the sake of optimality from other methods (i.e. creating new vector3s)
        relativisticRatios.x = Mathf.Lerp(relativisticRatios.x, Mathf.Min(Mathf.Abs(correctedSpeed.x), lightSpeed) / lightSpeed, smoothRelativity * Time.unscaledDeltaTime);
        relativisticRatios.y = Mathf.Lerp(relativisticRatios.y, Mathf.Min(Mathf.Abs(correctedSpeed.y), lightSpeed) / lightSpeed, smoothRelativity * Time.unscaledDeltaTime);
        relativisticRatios.z = Mathf.Lerp(relativisticRatios.z, Mathf.Min(Mathf.Abs(correctedSpeed.z), lightSpeed) / lightSpeed, smoothRelativity * Time.unscaledDeltaTime);

        //referencePoint = referenceSystem.transform.position + Vector3.down * referenceSystem.capsule.height * 0.5f;
        referencePoint = referenceSystem.transform.position;

        if(timeControl)
        {
            Time.timeScale = 1 - relativisticRatios.magnitude * relativisticTimeEffect;
            Time.fixedDeltaTime = originalPhysicsTimeStep * Time.timeScale;//Adjustment to keep physics updates smooth
        }
        scaledFixedDeltaTime = Time.fixedDeltaTime * Time.timeScale;
    }


}

